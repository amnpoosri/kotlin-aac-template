# Android Kotlin MVVM Architecture: Sample/Template App

This repository is written in kotlin. It contains a detailed sample/template app implementing MVVM software architecture, Dagger2, Android Architecture Component - Room, Live Data, and AndroidDebugDatabase.

<p align="center">
  <img src="https://user-images.githubusercontent.com/38126471/39483080-746049c8-4d9b-11e8-88f7-100503c17b63.png" width="250">
  <img src="https://user-images.githubusercontent.com/38126471/39483066-67bdcbb4-4d9b-11e8-8a36-216673035caf.jpg" width="250">
  <img src="https://user-images.githubusercontent.com/38126471/39483063-65cfe080-4d9b-11e8-99d7-4244c7dc9cb3.gif" width="250">
</p>
<br>
<p align="center">
  <img src="https://user-images.githubusercontent.com/38126471/39482987-31f5bfbe-4d9b-11e8-9bc6-e5e613b892a6.gif" width="250">
  <img src="https://user-images.githubusercontent.com/38126471/39483068-695db54c-4d9b-11e8-8604-de6d24c80779.gif" width="250">
</p>
<br>
<br>

## Understanding the File Structure
- `/constant` - Contains all the application constant.
- `/di` - Dependency providing classes using Dagger2.
- `/local` - Provide interface for local database
- `/manager` - Share Preference and other application managers
- `/model` - Contains all the data accessing and manipulating components.
- `/remote` - Contains APIs
- `/repository` - Contains interfaces and classes that do fetching and repositoring data
- `/template` - Contains template activity, fragment and viewmodel class.
- `/ui` - View classes.
- `/utils` - Utility classes.
- `/viewmodel` - Viewmodels classes.
- `MainApplication` - Android Base class

## Technologies

| Tech | Summary |
| --- | --- |
| [Android Architecture Component (Live Data)](https://developer.android.com/topic/libraries/architecture/livedata) | LiveData is a lifecycle-aware observable. LiveData makes it easy to keep what's showing on screen in sync with the data. |
| [Android Architecture Component (Room persistence)](https://developer.android.com/topic/libraries/architecture/room) | Room persistence library provides an abstraction layer over SQLite to allow fluent database access. |
| [Android Architecture Component (View Model)](https://developer.android.com/topic/libraries/architecture/viewmodel) | The ViewModel class is designed to hold and manage UI-related data in a life-cycle conscious way. |
| [AndroidDebugDatabase]( https://github.com/amitshekhariitbhu/Android-Debug-Database) | Android Debug Database is a powerful library for debugging databases and shared preferences in Android applications. |
| [AsyncListDiffer](https://developer.android.com/reference/android/support/v7/recyclerview/extensions/AsyncListDiffer) | A helper for computing the difference between two lists via DiffUtil on a background thread. |
| [Dagger 2](https://google.github.io/dagger/users-guide) | Dependency Injection Library. |
| [Glide](https://github.com/bumptech/glide) | Android Image loader library. |
| [Kotlin](https://kotlinlang.org/docs/tutorials/kotlin-android.html) | Kotlin is a statically typed programming language that runs on the Java virtual machine . |
| [List Adapter](https://developer.android.com/reference/android/support/v7/recyclerview/extensions/ListAdapter) | ListAdapter is a new class bundled in the 27.1.0 support library and simplifies the code required to work with RecyclerViews. |
| [Retrofit](http://square.github.io/retrofit) | A type-safe HTTP client for Android and Java  |

## How to debug database
    1. Connect Android device to your computer
    2. Run the project
    2. Click local address generated in logcat
    3. Go to terminal and type in the command to forward tcp port
      ```
      $ adb forward tcp:8080 tcp:8080
      ```
