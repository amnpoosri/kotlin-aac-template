package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u000b\u001a\u00020\fH\u0002J\u0010\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\b\u0010\u0010\u001a\u00020\fH\u0002J\u0006\u0010\u0011\u001a\u00020\u0000J\u0012\u0010\u0012\u001a\u00020\f2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J&\u0010\u0015\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J\"\u0010\u001a\u001a\u00020\f2\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0016\u0010 \u001a\u00020\f2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"H\u0002J\u0010\u0010$\u001a\u00020\f2\u0006\u0010%\u001a\u00020&H\u0002J\u0010\u0010\'\u001a\u00020\f2\u0006\u0010%\u001a\u00020&H\u0002R\u0010\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/fragment/LoginFragment;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/fragment/base/BaseFragment;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog$DialogSingleButtonClickListener;", "()V", "binding", "error/NonExistentClass", "Lerror/NonExistentClass;", "loginfragmentViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/fragment/LoginFragmentViewModel;", "progressDialog", "Landroid/app/ProgressDialog;", "goToMainPage", "", "hideKeyboard", "view", "Landroid/view/View;", "initial", "newInstance", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDialogSingleButtonClickListener", "dialog", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog;", "v", "request", "", "onLoginResult", "result", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/login/LoginResult;", "setPasswordError", "errorText", "", "setUsernameError", "app_debug"})
public final class LoginFragment extends com.apdevboost.amnuaychaipoosri.kotlintemplate.fragment.base.BaseFragment implements com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.DialogSingleButtonClickListener {
    private error.NonExistentClass binding;
    private com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment.LoginFragmentViewModel loginfragmentViewModel;
    private android.app.ProgressDialog progressDialog;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.fragment.LoginFragment newInstance() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initial() {
    }
    
    private final void setUsernameError(java.lang.String errorText) {
    }
    
    private final void setPasswordError(java.lang.String errorText) {
    }
    
    private final void onLoginResult(com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult> result) {
    }
    
    private final void goToMainPage() {
    }
    
    /**
     * * Hides the soft keyboard
     */
    private final void hideKeyboard(android.view.View view) {
    }
    
    @java.lang.Override()
    public void onDialogSingleButtonClickListener(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog dialog, @org.jetbrains.annotations.Nullable()
    android.view.View v, int request) {
    }
    
    public LoginFragment() {
        super();
    }
}