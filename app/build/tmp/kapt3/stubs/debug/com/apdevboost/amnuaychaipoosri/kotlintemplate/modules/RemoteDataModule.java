package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\b\u0010\u000f\u001a\u00020\fH\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/modules/RemoteDataModule;", "", "baseUrl", "", "(Ljava/lang/String;)V", "getBaseUrl", "()Ljava/lang/String;", "getDefaultHttpLoggingInterceptor", "Lokhttp3/logging/HttpLoggingInterceptor;", "provideApi", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/Api;", "retrofit", "Lretrofit2/Retrofit;", "provideLoginApi", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/LoginApi;", "provideRetrofit", "app_debug"})
@dagger.Module()
public final class RemoteDataModule {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String baseUrl = null;
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi provideLoginApi(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api provideApi(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final retrofit2.Retrofit provideRetrofit() {
        return null;
    }
    
    private final okhttp3.logging.HttpLoggingInterceptor getDefaultHttpLoggingInterceptor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBaseUrl() {
        return null;
    }
    
    public RemoteDataModule(@org.jetbrains.annotations.NotNull()
    java.lang.String baseUrl) {
        super();
    }
}