package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\"\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00060\u00052\u0006\u0010\t\u001a\u00020\nH&J\"\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00060\u00052\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\f"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepository;", "", "deleteDrinkDatabase", "", "getDrinkList", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "isForceFetch", "", "getDrinkListNoRoom", "app_debug"})
public abstract interface DrinkRepository {
    
    @org.jetbrains.annotations.NotNull()
    public abstract android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>>> getDrinkList(boolean isForceFetch);
    
    @org.jetbrains.annotations.NotNull()
    public abstract android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>>> getDrinkListNoRoom(boolean isForceFetch);
    
    public abstract void deleteDrinkDatabase();
}