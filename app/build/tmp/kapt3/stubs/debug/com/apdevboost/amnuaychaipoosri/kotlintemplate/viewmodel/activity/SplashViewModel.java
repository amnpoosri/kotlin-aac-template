package com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\nR\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u000b"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/SplashViewModel;", "Landroid/arch/lifecycle/ViewModel;", "()V", "userDataManager", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "getUserDataManager", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "setUserDataManager", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;)V", "isUserLogin", "", "app_debug"})
public final class SplashViewModel extends android.arch.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager userDataManager;
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager getUserDataManager() {
        return null;
    }
    
    public final void setUserDataManager(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager p0) {
    }
    
    public final boolean isUserLogin() {
        return false;
    }
    
    public SplashViewModel() {
        super();
    }
}