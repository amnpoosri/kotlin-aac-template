package com.apdevboost.amnuaychaipoosri.kotlintemplate.local;

import java.lang.System;

@android.arch.persistence.room.Dao()
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J#\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\'\u00a2\u0006\u0002\u0010\bJ!\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\f\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkIngredientDao;", "", "getDrinkIngredientList", "Landroid/arch/lifecycle/LiveData;", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;", "drinkId", "", "(Ljava/lang/Long;)Landroid/arch/lifecycle/LiveData;", "insertOrUpdateDrinkIngredientList", "", "drinkIngredientList", "", "([Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;)V", "app_debug"})
public abstract interface DrinkIngredientDao {
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void insertOrUpdateDrinkIngredientList(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient... drinkIngredientList);
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "SELECT * FROM drink_ingredient WHERE id = (:drinkId)")
    public abstract android.arch.lifecycle.LiveData<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient>> getDrinkIngredientList(@org.jetbrains.annotations.Nullable()
    java.lang.Long drinkId);
}