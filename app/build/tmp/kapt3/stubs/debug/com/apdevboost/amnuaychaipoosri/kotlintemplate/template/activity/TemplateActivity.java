package com.apdevboost.amnuaychaipoosri.kotlintemplate.template.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u00020\rH\u0002J\u0012\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014R\u0010\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u001b\u0010\u0006\u001a\u00020\u00078BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\t\u00a8\u0006\u0011"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/template/activity/TemplateActivity;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/activity/base/BaseActivity;", "()V", "binding", "error/NonExistentClass", "Lerror/NonExistentClass;", "viewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/template/viewmodel/TemplateViewModel;", "getViewModel", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/template/viewmodel/TemplateViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "initial", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class TemplateActivity extends com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.base.BaseActivity {
    private final kotlin.Lazy viewModel$delegate = null;
    private error.NonExistentClass binding;
    private java.util.HashMap _$_findViewCache;
    
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.template.viewmodel.TemplateViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initial() {
    }
    
    public TemplateActivity() {
        super();
    }
}