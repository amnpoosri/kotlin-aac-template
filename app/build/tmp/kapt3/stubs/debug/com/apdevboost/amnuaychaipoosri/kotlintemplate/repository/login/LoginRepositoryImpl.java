package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J,\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/login/LoginRepositoryImpl;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/login/LoginRepository;", "remoteSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/LoginApi;", "localSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/LoginApi;Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;)V", "getLocalSource", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "getRemoteSource", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/LoginApi;", "login", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/login/LoginResult;", "isForceFetch", "", "username", "", "password", "app_debug"})
public final class LoginRepositoryImpl implements com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepository {
    @org.jetbrains.annotations.NotNull()
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi remoteSource = null;
    @org.jetbrains.annotations.NotNull()
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager localSource = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult>> login(boolean isForceFetch, @org.jetbrains.annotations.NotNull()
    java.lang.String username, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi getRemoteSource() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager getLocalSource() {
        return null;
    }
    
    public LoginRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi remoteSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager localSource) {
        super();
    }
}