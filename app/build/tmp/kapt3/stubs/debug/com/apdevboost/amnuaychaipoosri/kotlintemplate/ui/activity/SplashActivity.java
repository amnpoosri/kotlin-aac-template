package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0011H\u0002J\b\u0010\u0013\u001a\u00020\u0011H\u0002J\u0012\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\b\u0010\u0017\u001a\u00020\u0011H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\r\u00a8\u0006\u0018"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/activity/SplashActivity;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/activity/base/BaseActivity;", "()V", "TAG", "", "binding", "error/NonExistentClass", "Lerror/NonExistentClass;", "handler", "Landroid/os/Handler;", "splashViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/SplashViewModel;", "getSplashViewModel", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/SplashViewModel;", "splashViewModel$delegate", "Lkotlin/Lazy;", "goToLoginPage", "", "goToMainPage", "initial", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "proceedToNextPage", "app_debug"})
public final class SplashActivity extends com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.base.BaseActivity {
    private final java.lang.String TAG = "SplashActivity";
    private final android.os.Handler handler = null;
    private final kotlin.Lazy splashViewModel$delegate = null;
    private error.NonExistentClass binding;
    private java.util.HashMap _$_findViewCache;
    
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.SplashViewModel getSplashViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initial() {
    }
    
    private final void proceedToNextPage() {
    }
    
    private final void goToMainPage() {
    }
    
    private final void goToLoginPage() {
    }
    
    public SplashActivity() {
        super();
    }
}