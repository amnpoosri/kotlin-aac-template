package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0010H\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/modules/RepositoryModule;", "", "()V", "drinkRepositoryImpl", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepository;", "localSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkDao;", "localDrinkIngredientSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkIngredientDao;", "remoteSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/Api;", "appExecutors", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/base/AppExecutors;", "provideLoginRepository", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/login/LoginRepository;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/LoginApi;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "app_debug"})
@dagger.Module()
public final class RepositoryModule {
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepository provideLoginRepository(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi remoteSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager localSource) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository drinkRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao localSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao localDrinkIngredientSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api remoteSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors appExecutors) {
        return null;
    }
    
    public RepositoryModule() {
        super();
    }
}