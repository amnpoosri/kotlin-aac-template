package com.apdevboost.amnuaychaipoosri.kotlintemplate.custom.base.state;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u000f\b\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u0014"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/custom/base/state/BundleSavedState;", "Landroid/view/View$BaseSavedState;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "superState", "Landroid/os/Parcelable;", "(Landroid/os/Parcelable;)V", "bundle", "Landroid/os/Bundle;", "getBundle", "()Landroid/os/Bundle;", "setBundle", "(Landroid/os/Bundle;)V", "writeToParcel", "", "out", "flags", "", "Companion", "app_debug"})
public final class BundleSavedState extends android.view.View.BaseSavedState {
    @org.jetbrains.annotations.NotNull()
    private android.os.Bundle bundle;
    @org.jetbrains.annotations.NotNull()
    private static final android.os.Parcelable.Creator<java.lang.Object> CREATOR = null;
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.custom.base.state.BundleSavedState.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final android.os.Bundle getBundle() {
        return null;
    }
    
    public final void setBundle(@org.jetbrains.annotations.NotNull()
    android.os.Bundle p0) {
    }
    
    @java.lang.Override()
    public void writeToParcel(@org.jetbrains.annotations.NotNull()
    android.os.Parcel out, int flags) {
    }
    
    public BundleSavedState(@org.jetbrains.annotations.NotNull()
    android.os.Parcel source) {
        super(null);
    }
    
    public BundleSavedState(@org.jetbrains.annotations.NotNull()
    android.os.Parcelable superState) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/custom/base/state/BundleSavedState$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "getCREATOR", "()Landroid/os/Parcelable$Creator;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final android.os.Parcelable.Creator<java.lang.Object> getCREATOR() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}