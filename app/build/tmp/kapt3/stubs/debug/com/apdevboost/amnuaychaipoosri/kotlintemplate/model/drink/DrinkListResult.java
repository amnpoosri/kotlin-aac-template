package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002R&\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkListResult;", "", "()V", "drinkList", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "getDrinkList", "()Ljava/util/List;", "setDrinkList", "(Ljava/util/List;)V", "app_debug"})
public final class DrinkListResult {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkList")
    private java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink> drinkList;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink> getDrinkList() {
        return null;
    }
    
    public final void setDrinkList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink> p0) {
    }
    
    /**
     * * No args constructor for use in serialization
     *     *
     */
    public DrinkListResult() {
        super();
    }
}