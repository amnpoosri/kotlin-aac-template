package com.apdevboost.amnuaychaipoosri.kotlintemplate.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH&\u00a8\u0006\u000e"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/component/AppComponent;", "", "inject", "", "application", "Landroid/app/Application;", "templateViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/template/viewmodel/TemplateViewModel;", "mainViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/MainViewModel;", "splashViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/SplashViewModel;", "loginFragmentViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/fragment/LoginFragmentViewModel;", "app_debug"})
@dagger.Component(modules = {com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.AppModule.class, com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule.class, com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RemoteDataModule.class, com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.AppExecutorsModule.class, com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RepositoryModule.class, com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.ConfigurationModule.class})
@com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
public abstract interface AppComponent {
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    android.app.Application application);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.SplashViewModel splashViewModel);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.MainViewModel mainViewModel);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.template.viewmodel.TemplateViewModel templateViewModel);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment.LoginFragmentViewModel loginFragmentViewModel);
}