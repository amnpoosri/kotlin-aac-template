package com.apdevboost.amnuaychaipoosri.kotlintemplate.local;

import java.lang.System;

@android.arch.persistence.room.Dao()
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\bg\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J!\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000b0\n\"\u00020\u000bH\'\u00a2\u0006\u0002\u0010\fJ!\u0010\r\u001a\u00020\b2\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\n\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\u000fJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u00032\u0006\u0010\u0011\u001a\u00020\u0012H\'\u00a8\u0006\u0013"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkDao;", "", "getAllDrinkWithDrinkIngredient", "Landroid/arch/lifecycle/LiveData;", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "getAlldrinkList", "insertOrUpdateDrinkIngredientList", "", "drinkIngredientList", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;", "([Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;)V", "insertOrUpdateDrinkList", "drinkList", "([Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;)V", "loadProduct", "drinkId", "", "app_debug"})
public abstract interface DrinkDao {
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    @android.arch.persistence.room.Transaction()
    public abstract void insertOrUpdateDrinkList(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink... drinkList);
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "SELECT * FROM drink")
    public abstract android.arch.lifecycle.LiveData<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>> getAlldrinkList();
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "SELECT * FROM drink WHERE drinkId = :drinkId")
    public abstract android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink> loadProduct(int drinkId);
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "SELECT * FROM drink INNER JOIN drink_ingredient ON drinkId")
    public abstract android.arch.lifecycle.LiveData<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>> getAllDrinkWithDrinkIngredient();
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void insertOrUpdateDrinkIngredientList(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient... drinkIngredientList);
}