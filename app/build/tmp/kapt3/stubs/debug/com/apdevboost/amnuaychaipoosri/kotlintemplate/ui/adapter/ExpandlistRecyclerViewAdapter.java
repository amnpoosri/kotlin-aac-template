package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0014\u0015B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\n\u001a\u00020\u0006H\u0016J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006H\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0006H\u0016J\u0018\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0006H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/adapter/ExpandlistRecyclerViewAdapter;", "Landroid/support/v7/recyclerview/extensions/ListAdapter;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "()V", "VIEWTYPE_HEADER", "", "VIEWTYPE_ITEM", "context", "Landroid/content/Context;", "getItemCount", "getItemViewType", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ExpandlistHeaderViewItemHolder", "ExpandlistViewItemHolder", "app_debug"})
public class ExpandlistRecyclerViewAdapter extends android.support.v7.recyclerview.extensions.ListAdapter<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient, android.support.v7.widget.RecyclerView.ViewHolder> {
    private final int VIEWTYPE_HEADER = 1;
    private final int VIEWTYPE_ITEM = 2;
    private android.content.Context context;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.support.v7.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    public ExpandlistRecyclerViewAdapter() {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\b\u001a\u00020\tR\u0013\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/adapter/ExpandlistRecyclerViewAdapter$ExpandlistHeaderViewItemHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "binding", "error/NonExistentClass", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/adapter/ExpandlistRecyclerViewAdapter;Lerror/NonExistentClass;)V", "getBinding", "()Lerror/NonExistentClass;", "Lerror/NonExistentClass;", "bind", "", "app_debug"})
    public final class ExpandlistHeaderViewItemHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final error.NonExistentClass binding = null;
        
        public final void bind() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final error.NonExistentClass getBinding() {
            return null;
        }
        
        public ExpandlistHeaderViewItemHolder(@org.jetbrains.annotations.NotNull()
        error.NonExistentClass binding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\b\u001a\u00020\tR\u0013\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/adapter/ExpandlistRecyclerViewAdapter$ExpandlistViewItemHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "binding", "error/NonExistentClass", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/adapter/ExpandlistRecyclerViewAdapter;Lerror/NonExistentClass;)V", "getBinding", "()Lerror/NonExistentClass;", "Lerror/NonExistentClass;", "bind", "", "app_debug"})
    public final class ExpandlistViewItemHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final error.NonExistentClass binding = null;
        
        public final void bind() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final error.NonExistentClass getBinding() {
            return null;
        }
        
        public ExpandlistViewItemHolder(@org.jetbrains.annotations.NotNull()
        error.NonExistentClass binding) {
            super(null);
        }
    }
}