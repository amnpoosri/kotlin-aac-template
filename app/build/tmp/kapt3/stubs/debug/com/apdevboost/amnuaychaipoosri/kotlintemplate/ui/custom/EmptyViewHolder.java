package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\b\u001a\u00020\tR\u0013\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/EmptyViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "binding", "error/NonExistentClass", "(Lerror/NonExistentClass;)V", "getBinding", "()Lerror/NonExistentClass;", "Lerror/NonExistentClass;", "bind", "", "app_debug"})
public final class EmptyViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    @org.jetbrains.annotations.NotNull()
    private final error.NonExistentClass binding = null;
    
    public final void bind() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final error.NonExistentClass getBinding() {
        return null;
    }
    
    public EmptyViewHolder(@org.jetbrains.annotations.NotNull()
    error.NonExistentClass binding) {
        super(null);
    }
}