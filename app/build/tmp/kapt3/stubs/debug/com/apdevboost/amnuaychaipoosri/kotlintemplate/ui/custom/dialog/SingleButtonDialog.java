package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 !2\u00020\u0001:\u0003 !\"B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0002J\u0012\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\u0012\u0010\u0013\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J&\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\u0012\u0010\u001a\u001a\u00020\u000f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0012H\u0002J\u0012\u0010\u001c\u001a\u00020\u000f2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0012H\u0002J\u0010\u0010\u001e\u001a\u00020\u000f2\u0006\u0010\u001f\u001a\u00020\u0012H\u0016R\u0010\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/custom/base/BaseDialog;", "()V", "binding", "error/NonExistentClass", "Lerror/NonExistentClass;", "buttonName", "", "dialogClickListener", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog$DialogSingleButtonClickListener;", "request", "", "subTitle", "title", "initial", "", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreate", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onRestoreInstanceState", "saveInstanceState", "onRestorenonInstanceState", "bundle", "onSaveInstanceState", "outState", "Builder", "Companion", "DialogSingleButtonClickListener", "app_debug"})
public final class SingleButtonDialog extends com.apdevboost.amnuaychaipoosri.kotlintemplate.custom.base.BaseDialog {
    private error.NonExistentClass binding;
    private com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.DialogSingleButtonClickListener dialogClickListener;
    private java.lang.String title;
    private java.lang.String subTitle;
    private java.lang.String buttonName;
    private int request;
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initial() {
    }
    
    @java.lang.Override()
    public void onSaveInstanceState(@org.jetbrains.annotations.NotNull()
    android.os.Bundle outState) {
    }
    
    private final void onRestoreInstanceState(android.os.Bundle saveInstanceState) {
    }
    
    private final void onRestorenonInstanceState(android.os.Bundle bundle) {
    }
    
    public SingleButtonDialog() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0006J\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0004J\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0004J\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog$Builder;", "", "()V", "buttonName", "", "dialogClickListener", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog$DialogSingleButtonClickListener;", "request", "", "subTitle", "title", "build", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog;", "setOnButtonClickListener", "dialogSingleButtonClickListener", "setSubTitle", "setTitle", "setbuttonName", "app_debug"})
    public static class Builder {
        private java.lang.String title;
        private java.lang.String subTitle;
        private java.lang.String buttonName;
        private int request;
        private com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.DialogSingleButtonClickListener dialogClickListener;
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog build() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.Builder setTitle(@org.jetbrains.annotations.NotNull()
        java.lang.String title) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.Builder setSubTitle(@org.jetbrains.annotations.NotNull()
        java.lang.String subTitle) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.Builder setbuttonName(@org.jetbrains.annotations.NotNull()
        java.lang.String buttonName) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.Builder request(int request) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.Builder setOnButtonClickListener(@org.jetbrains.annotations.NotNull()
        com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog.DialogSingleButtonClickListener dialogSingleButtonClickListener) {
            return null;
        }
        
        public Builder() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\tH&\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog$DialogSingleButtonClickListener;", "", "onDialogSingleButtonClickListener", "", "dialog", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog;", "v", "Landroid/view/View;", "request", "", "app_debug"})
    public static abstract interface DialogSingleButtonClickListener {
        
        public abstract void onDialogSingleButtonClickListener(@org.jetbrains.annotations.NotNull()
        com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog dialog, @org.jetbrains.annotations.Nullable()
        android.view.View v, int request);
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog$Companion;", "", "()V", "newInstance", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/custom/dialog/SingleButtonDialog;", "title", "", "subTitle", "buttonName", "request", "", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog newInstance(@org.jetbrains.annotations.Nullable()
        java.lang.String title, @org.jetbrains.annotations.Nullable()
        java.lang.String subTitle, @org.jetbrains.annotations.Nullable()
        java.lang.String buttonName, int request) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}