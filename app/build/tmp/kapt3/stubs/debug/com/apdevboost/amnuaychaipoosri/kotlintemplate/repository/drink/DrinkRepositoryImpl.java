package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\"\u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u00180\u00170\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\"\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u00180\u00170\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0012\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u00180\u0016R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006\u001e"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepositoryImpl;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepository;", "localSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkDao;", "localDrinkIngredientSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkIngredientDao;", "remoteSource", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/Api;", "appExecutors", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/base/AppExecutors;", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkDao;Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkIngredientDao;Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/Api;Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/base/AppExecutors;)V", "getAppExecutors", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/base/AppExecutors;", "getLocalDrinkIngredientSource", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkIngredientDao;", "getLocalSource", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkDao;", "getRemoteSource", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/Api;", "deleteDrinkDatabase", "", "getDrinkList", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "isForceFetch", "", "getDrinkListNoRoom", "getDrinksWithIngredients", "app_debug"})
public final class DrinkRepositoryImpl implements com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository {
    @org.jetbrains.annotations.NotNull()
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao localSource = null;
    @org.jetbrains.annotations.NotNull()
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao localDrinkIngredientSource = null;
    @org.jetbrains.annotations.NotNull()
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api remoteSource = null;
    @org.jetbrains.annotations.NotNull()
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors appExecutors = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>>> getDrinkListNoRoom(boolean isForceFetch) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>>> getDrinkList(boolean isForceFetch) {
        return null;
    }
    
    @java.lang.Override()
    public void deleteDrinkDatabase() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.LiveData<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>> getDrinksWithIngredients() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao getLocalSource() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao getLocalDrinkIngredientSource() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api getRemoteSource() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors getAppExecutors() {
        return null;
    }
    
    public DrinkRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao localSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao localDrinkIngredientSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api remoteSource, @org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors appExecutors) {
        super();
    }
}