package com.apdevboost.amnuaychaipoosri.kotlintemplate.utils;

import java.lang.System;

/**
 * * Provides utility methods for working with the device screen.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004J\u0016\u0010\t\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0004J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0014"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/utils/ScreenUtils;", "", "()V", "convertDIPToPixels", "", "context", "Landroid/content/Context;", "dip", "", "convertPixelsToDIP", "pixels", "getScreenDimensionsInDIP", "Landroid/graphics/Point;", "getScreenSize", "hasLargeScreen", "", "hasNormalScreen", "hasSmallScreen", "hasXLargeScreen", "isInLandscapeOrientation", "app_debug"})
public final class ScreenUtils {
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.utils.ScreenUtils INSTANCE = null;
    
    /**
     * * Converts the given device independent pixels (DIP) value into the corresponding pixels
     *     * value for the current screen.
     *     *
     *     * @param context Context instance
     *     * @param dip The DIP value to convert
     *     *
     *     * @return The pixels value for the current screen of the given DIP value.
     */
    public final int convertDIPToPixels(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int dip) {
        return 0;
    }
    
    /**
     * * Converts the given device independent pixels (DIP) value into the corresponding pixels
     *     * value for the current screen.
     *     *
     *     * @param context Context instance
     *     * @param dip The DIP value to convert
     *     *
     *     * @return The pixels value for the current screen of the given DIP value.
     */
    public final int convertDIPToPixels(@org.jetbrains.annotations.NotNull()
    android.content.Context context, float dip) {
        return 0;
    }
    
    /**
     * * Converts the given pixels value into the corresponding device independent pixels (DIP)
     *     * value for the current screen.
     *     *
     *     * @param context Context instance
     *     * @param pixels The pixels value to convert
     *     *
     *     * @return The DIP value for the current screen of the given pixels value.
     */
    public final float convertPixelsToDIP(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int pixels) {
        return 0.0F;
    }
    
    /**
     * * Returns the current screen dimensions in device independent pixels (DIP) as a [Point] object where
     *     * [Point.x] is the screen width and [Point.y] is the screen height.
     *     *
     *     * @param context Context instance
     *     *
     *     * @return The current screen dimensions in DIP.
     */
    @org.jetbrains.annotations.NotNull()
    public final android.graphics.Point getScreenDimensionsInDIP(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    /**
     * * @param context Context instance
     *     *
     *     * @return [true] if the device is in landscape orientation, [false] otherwise.
     */
    public final boolean isInLandscapeOrientation(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * * @param context Context instance
     *     *
     *     * @return [true] if the device has a small screen, [false] otherwise.
     */
    public final boolean hasSmallScreen(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * * @param context Context instance
     *     *
     *     * @return [true] if the device has a normal screen, [false] otherwise.
     */
    public final boolean hasNormalScreen(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * * @param context Context instance
     *     *
     *     * @return [true] if the device has a large screen, [false] otherwise.
     */
    public final boolean hasLargeScreen(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * * @param context Context instance
     *     *
     *     * @return [true] if the device has an extra large screen, [false] otherwise.
     */
    public final boolean hasXLargeScreen(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * * The size of the screen, one of 4 possible values:
     *     *
     *     *
     *     *  * http://developer.android.com/reference/android/content/res/Configuration.html#SCREENLAYOUT_SIZE_SMALL
     *     *  * http://developer.android.com/reference/android/content/res/Configuration.html#SCREENLAYOUT_SIZE_NORMAL
     *     *  * http://developer.android.com/reference/android/content/res/Configuration.html#SCREENLAYOUT_SIZE_LARGE
     *     *  * http://developer.android.com/reference/android/content/res/Configuration.html#SCREENLAYOUT_SIZE_XLARGE
     *     *
     *     *
     *     * See http://developer.android.com/reference/android/content/res/Configuration.html#screenLayout for more details.
     *     *
     *     * @param context Context instance
     *     *
     *     * @return The size of the screen
     */
    public final int getScreenSize(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return 0;
    }
    
    private ScreenUtils() {
        super();
    }
}