package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink;

import java.lang.System;

@android.arch.persistence.room.Entity(tableName = "drink_ingredient", foreignKeys = {@android.arch.persistence.room.ForeignKey(entity = com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink.class, childColumns = {"drink_id"}, onDelete = 5, parentColumns = {"drinkId"})}, indices = {@android.arch.persistence.room.Index(value = {"id"}), @android.arch.persistence.room.Index(value = {"drink_id"}), @android.arch.persistence.room.Index(value = {"drinkIngredientId"})})
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0010\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R \u0010\u0017\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0014\"\u0004\b\u0019\u0010\u0016R\"\u0010\u001a\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u001b\u0010\u0006\"\u0004\b\u001c\u0010\b\u00a8\u0006\u001d"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;", "", "()V", "drinkId", "", "getDrinkId", "()Ljava/lang/Long;", "setDrinkId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "drinkIngredientId", "", "getDrinkIngredientId", "()Ljava/lang/Integer;", "setDrinkIngredientId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "drinkIngredientName", "", "getDrinkIngredientName", "()Ljava/lang/String;", "setDrinkIngredientName", "(Ljava/lang/String;)V", "drinkIngredientWeight", "getDrinkIngredientWeight", "setDrinkIngredientWeight", "id", "getId", "setId", "app_debug"})
public final class DrinkIngredient {
    @org.jetbrains.annotations.Nullable()
    @android.arch.persistence.room.PrimaryKey(autoGenerate = true)
    private java.lang.Long id;
    @org.jetbrains.annotations.Nullable()
    @android.arch.persistence.room.ColumnInfo(name = "drink_id")
    private java.lang.Long drinkId;
    @org.jetbrains.annotations.Nullable()
    @android.arch.persistence.room.ColumnInfo(name = "drinkIngredientId")
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkIngredientId")
    private java.lang.Integer drinkIngredientId;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkIngredientName")
    private java.lang.String drinkIngredientName;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkIngredientWeight")
    private java.lang.String drinkIngredientWeight;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getDrinkId() {
        return null;
    }
    
    public final void setDrinkId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getDrinkIngredientId() {
        return null;
    }
    
    public final void setDrinkIngredientId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDrinkIngredientName() {
        return null;
    }
    
    public final void setDrinkIngredientName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDrinkIngredientWeight() {
        return null;
    }
    
    public final void setDrinkIngredientWeight(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public DrinkIngredient() {
        super();
    }
}