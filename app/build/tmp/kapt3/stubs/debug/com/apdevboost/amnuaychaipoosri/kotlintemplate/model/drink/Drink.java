package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink;

import java.lang.System;

@android.arch.persistence.room.Entity(tableName = "drink", indices = {@android.arch.persistence.room.Index(value = {"drinkId"})})
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0016\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B=\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u00a2\u0006\u0002\u0010\fR \u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\b\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u000f\"\u0004\b\u001a\u0010\u0011R&\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR \u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u000f\"\u0004\b \u0010\u0011\u00a8\u0006!"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "", "()V", "drinkId", "", "drinkImageUrl", "", "drinkTitle", "drinkDescription", "drinkIngredientList", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkIngredient;", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "drinkAlcoholdContent", "getDrinkAlcoholdContent", "()Ljava/lang/String;", "setDrinkAlcoholdContent", "(Ljava/lang/String;)V", "getDrinkDescription", "setDrinkDescription", "getDrinkId", "()Ljava/lang/Long;", "setDrinkId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getDrinkImageUrl", "setDrinkImageUrl", "getDrinkIngredientList", "()Ljava/util/List;", "setDrinkIngredientList", "(Ljava/util/List;)V", "getDrinkTitle", "setDrinkTitle", "app_debug"})
public final class Drink {
    @org.jetbrains.annotations.Nullable()
    @android.arch.persistence.room.ColumnInfo(name = "drinkId")
    @android.arch.persistence.room.PrimaryKey()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkId")
    private java.lang.Long drinkId;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkImageUrl")
    private java.lang.String drinkImageUrl;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkAlcoholContent")
    private java.lang.String drinkAlcoholdContent;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkTitle")
    private java.lang.String drinkTitle;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkDescription")
    private java.lang.String drinkDescription;
    @org.jetbrains.annotations.Nullable()
    @android.arch.persistence.room.Ignore()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "drinkIngredientList")
    private java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient> drinkIngredientList;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getDrinkId() {
        return null;
    }
    
    public final void setDrinkId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDrinkImageUrl() {
        return null;
    }
    
    public final void setDrinkImageUrl(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDrinkAlcoholdContent() {
        return null;
    }
    
    public final void setDrinkAlcoholdContent(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDrinkTitle() {
        return null;
    }
    
    public final void setDrinkTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDrinkDescription() {
        return null;
    }
    
    public final void setDrinkDescription(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient> getDrinkIngredientList() {
        return null;
    }
    
    public final void setDrinkIngredientList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient> p0) {
    }
    
    /**
     * * No args constructor for use in serialization
     *     *
     */
    public Drink() {
        super();
    }
    
    /**
     * *
     *     * @param drinkId
     *     * @param drinkImageUrl
     *     * @param drinkTitle
     *     * @param drinkDescription
     *     * @param drinkIngredientList
     */
    public Drink(@org.jetbrains.annotations.Nullable()
    java.lang.Long drinkId, @org.jetbrains.annotations.NotNull()
    java.lang.String drinkImageUrl, @org.jetbrains.annotations.Nullable()
    java.lang.String drinkTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String drinkDescription, @org.jetbrains.annotations.Nullable()
    java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient> drinkIngredientList) {
        super();
    }
}