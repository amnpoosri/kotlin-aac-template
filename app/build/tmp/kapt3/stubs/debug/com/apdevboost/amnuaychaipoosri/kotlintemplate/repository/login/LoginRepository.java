package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J,\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH&\u00a8\u0006\u000b"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/login/LoginRepository;", "", "login", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/login/LoginResult;", "isForceFetch", "", "username", "", "password", "app_debug"})
public abstract interface LoginRepository {
    
    @org.jetbrains.annotations.NotNull()
    public abstract android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult>> login(boolean isForceFetch, @org.jetbrains.annotations.NotNull()
    java.lang.String username, @org.jetbrains.annotations.NotNull()
    java.lang.String password);
}