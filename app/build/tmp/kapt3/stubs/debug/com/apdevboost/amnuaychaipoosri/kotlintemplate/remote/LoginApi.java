package com.apdevboost.amnuaychaipoosri.kotlintemplate.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J.\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00040\u00032\b\b\u0001\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\bH\'\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/LoginApi;", "", "login", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/ApiResponse;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/BaseResponse;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/login/LoginResult;", "username", "", "password", "app_debug"})
public abstract interface LoginApi {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "v2/5ae41f342f00004b0028e771")
    public abstract android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.ApiResponse<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.BaseResponse<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult>>> login(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "username")
    java.lang.String username, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "password")
    java.lang.String password);
}