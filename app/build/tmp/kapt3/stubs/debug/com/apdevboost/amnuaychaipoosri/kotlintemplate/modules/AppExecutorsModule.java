package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/modules/AppExecutorsModule;", "", "()V", "provideAppExecutors", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/base/AppExecutors;", "app_debug"})
@dagger.Module()
public final class AppExecutorsModule {
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors provideAppExecutors() {
        return null;
    }
    
    public AppExecutorsModule() {
        super();
    }
}