package com.apdevboost.amnuaychaipoosri.kotlintemplate.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0002J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0006H\u0016\u00a8\u0006\u000b"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/utils/PrettyHttpLogger;", "Lokhttp3/logging/HttpLoggingInterceptor$Logger;", "()V", "largeLog", "", "tag", "", "content", "log", "message", "Companion", "app_debug"})
public final class PrettyHttpLogger implements okhttp3.logging.HttpLoggingInterceptor.Logger {
    private static final java.lang.String TAG = null;
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.utils.PrettyHttpLogger.Companion Companion = null;
    
    @java.lang.Override()
    public void log(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    private final void largeLog(java.lang.String tag, java.lang.String content) {
    }
    
    public PrettyHttpLogger() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/utils/PrettyHttpLogger$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}