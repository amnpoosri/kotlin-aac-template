package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0011H\u0002J\b\u0010\u0013\u001a\u00020\u0011H\u0002J\b\u0010\u0014\u001a\u00020\u0011H\u0002J\u0012\u0010\u0015\u001a\u00020\u00112\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u001e\u0010\u001c\u001a\u00020\u00112\u0014\u0010\u001d\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u001f\u0018\u00010\u001eH\u0002J\u0010\u0010!\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020#H\u0016J\u0010\u0010$\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0012\u0010%\u001a\u00020\u00112\b\u0010&\u001a\u0004\u0018\u00010\'H\u0002J\u0012\u0010(\u001a\u00020\u00112\b\u0010)\u001a\u0004\u0018\u00010\u0017H\u0014J \u0010*\u001a\u00020\u00112\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020 0+j\b\u0012\u0004\u0012\u00020 `,H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0004\n\u0002\u0010\tR\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\r\u00a8\u0006-"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/activity/MainActivity;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/activity/base/BaseActivity;", "()V", "TAG", "", "adapter", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/ui/adapter/DrinkRecyclerViewAdapter;", "binding", "error/NonExistentClass", "Lerror/NonExistentClass;", "mainViewModel", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/MainViewModel;", "getMainViewModel", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/MainViewModel;", "mainViewModel$delegate", "Lkotlin/Lazy;", "attemptToGoLoginPage", "", "getDrinkListData", "goToLoginPage", "initial", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onGetDrinkListDataResult", "data", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onRestoreSavedInstanceState", "onRetrievedDataFromBundle", "intent", "Landroid/content/Intent;", "onSaveInstanceState", "outState", "updateDrinkAdapter", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "app_debug"})
public final class MainActivity extends com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.base.BaseActivity {
    private final java.lang.String TAG = "MainActivity";
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.adapter.DrinkRecyclerViewAdapter adapter = null;
    private final kotlin.Lazy mainViewModel$delegate = null;
    private error.NonExistentClass binding;
    private java.util.HashMap _$_findViewCache;
    
    private final com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.MainViewModel getMainViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void onRestoreSavedInstanceState(android.os.Bundle savedInstanceState) {
    }
    
    private final void onRetrievedDataFromBundle(android.content.Intent intent) {
    }
    
    @java.lang.Override()
    protected void onSaveInstanceState(@org.jetbrains.annotations.Nullable()
    android.os.Bundle outState) {
    }
    
    private final void initial() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void attemptToGoLoginPage() {
    }
    
    private final void goToLoginPage() {
    }
    
    private final void getDrinkListData() {
    }
    
    private final void updateDrinkAdapter(java.util.ArrayList<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink> data) {
    }
    
    private final void onGetDrinkListDataResult(com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>> data) {
    }
    
    public MainActivity() {
        super();
    }
}