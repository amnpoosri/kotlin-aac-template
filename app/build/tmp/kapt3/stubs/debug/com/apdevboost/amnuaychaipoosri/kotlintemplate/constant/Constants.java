package com.apdevboost.amnuaychaipoosri.kotlintemplate.constant;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/constant/Constants;", "", "()V", "Companion", "app_debug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String APP = "KotlinACCTemplate";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEVELOPMENT_BASE_URL = "http://www.mocky.io/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TESTING_BASE_URL = "http://www.mocky.io/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCTION_BASE_URL = "http://www.mocky.io/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DB_FILE_NAME = "common_db";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHARE_PREF_NAME = "local";
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.constant.Constants.Companion Companion = null;
    
    public Constants() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/constant/Constants$Companion;", "", "()V", "APP", "", "DB_FILE_NAME", "DEVELOPMENT_BASE_URL", "PRODUCTION_BASE_URL", "SHARE_PREF_NAME", "TESTING_BASE_URL", "TIMESTAMP_FORMAT", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}