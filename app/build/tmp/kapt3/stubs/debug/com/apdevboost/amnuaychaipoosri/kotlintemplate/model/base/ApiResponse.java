package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base;

import java.lang.System;

/**
 * * Common class used by API responses.
 * *
 * * @param <T>
 * </T> 
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u0000 \u0019*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001\u0019B#\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00018\u0000\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bB\u000f\b\u0016\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bB\u0015\b\u0016\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\r\u00a2\u0006\u0002\u0010\u000eR\u0015\u0010\u0005\u001a\u0004\u0018\u00018\u0000\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0018\u00a8\u0006\u001a"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/ApiResponse;", "T", "", "code", "", "body", "errorMessage", "", "(ILjava/lang/Object;Ljava/lang/String;)V", "error", "", "(Ljava/lang/Throwable;)V", "response", "Lretrofit2/Response;", "(Lretrofit2/Response;)V", "getBody", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getCode", "()I", "getErrorMessage", "()Ljava/lang/String;", "isSuccessful", "", "()Z", "Companion", "app_debug"})
public final class ApiResponse<T extends java.lang.Object> {
    private final int code = 0;
    @org.jetbrains.annotations.Nullable()
    private final T body = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String errorMessage = null;
    private static final java.lang.String TAG = null;
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.ApiResponse.Companion Companion = null;
    
    public final int getCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final T getBody() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getErrorMessage() {
        return null;
    }
    
    public final boolean isSuccessful() {
        return false;
    }
    
    public ApiResponse(int code, @org.jetbrains.annotations.Nullable()
    T body, @org.jetbrains.annotations.Nullable()
    java.lang.String errorMessage) {
        super();
    }
    
    public ApiResponse(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable error) {
        super();
    }
    
    public ApiResponse(@org.jetbrains.annotations.NotNull()
    retrofit2.Response<T> response) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/ApiResponse$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}