
package android.databinding;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.BR;
class DataBinderMapperImpl extends android.databinding.DataBinderMapper {
    public DataBinderMapperImpl() {
    }
    @Override
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_login:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_login_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivityLoginBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_expandlist_header_item:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/viewholder_expandlist_header_item_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderExpandlistHeaderItemBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for viewholder_expandlist_header_item is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.dialog_single_button:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/dialog_single_button_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.DialogSingleButtonBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for dialog_single_button is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_main:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_main_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivityMainBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_empty:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/viewholder_empty_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderEmptyBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for viewholder_empty is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_expandlist_item:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/viewholder_expandlist_item_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderExpandlistItemBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for viewholder_expandlist_item is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.fragment_template:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_template_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.FragmentTemplateBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_template is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_card_item:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/viewholder_card_item_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderCardItemBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for viewholder_card_item is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_template:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_template_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivityTemplateBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_template is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.widget_loading:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/widget_loading_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.WidgetLoadingBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for widget_loading is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.fragment_login:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_login_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.FragmentLoginBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
                }
                case com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_splash:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_splash_0".equals(tag)) {
                            return new com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivitySplashBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
                }
        }
        return null;
    }
    @Override
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    @Override
    public int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case -237232145: {
                if(tag.equals("layout/activity_login_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_login;
                }
                break;
            }
            case 542398904: {
                if(tag.equals("layout/viewholder_expandlist_header_item_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_expandlist_header_item;
                }
                break;
            }
            case -94558200: {
                if(tag.equals("layout/dialog_single_button_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.dialog_single_button;
                }
                break;
            }
            case 423753077: {
                if(tag.equals("layout/activity_main_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_main;
                }
                break;
            }
            case -1127615723: {
                if(tag.equals("layout/viewholder_empty_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_empty;
                }
                break;
            }
            case 1447881346: {
                if(tag.equals("layout/viewholder_expandlist_item_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_expandlist_item;
                }
                break;
            }
            case -1869242219: {
                if(tag.equals("layout/fragment_template_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.fragment_template;
                }
                break;
            }
            case 1419246570: {
                if(tag.equals("layout/viewholder_card_item_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_card_item;
                }
                break;
            }
            case 892138102: {
                if(tag.equals("layout/activity_template_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_template;
                }
                break;
            }
            case -340386313: {
                if(tag.equals("layout/widget_loading_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.widget_loading;
                }
                break;
            }
            case -986431952: {
                if(tag.equals("layout/fragment_login_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.fragment_login;
                }
                break;
            }
            case 1573928931: {
                if(tag.equals("layout/activity_splash_0")) {
                    return com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.activity_splash;
                }
                break;
            }
        }
        return 0;
    }
    @Override
    public String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"};
    }
}