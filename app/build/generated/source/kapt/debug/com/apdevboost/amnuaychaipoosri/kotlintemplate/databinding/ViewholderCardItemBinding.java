package com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ViewholderCardItemBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.viewholderCardItemMainLL, 1);
        sViewsWithIds.put(R.id.viewholderCardItemIV, 2);
        sViewsWithIds.put(R.id.viewholderCardItemTitleTV, 3);
        sViewsWithIds.put(R.id.viewholderCardItemAlcoholTV, 4);
        sViewsWithIds.put(R.id.viewholderCardItemDescriptionTV, 5);
        sViewsWithIds.put(R.id.viewholderCardItemArrowIV, 6);
        sViewsWithIds.put(R.id.viewholderCardItemExpandlistRV, 7);
    }
    // views
    @NonNull
    private final android.support.v7.widget.CardView mboundView0;
    @NonNull
    public final android.widget.TextView viewholderCardItemAlcoholTV;
    @NonNull
    public final android.widget.ImageView viewholderCardItemArrowIV;
    @NonNull
    public final android.widget.TextView viewholderCardItemDescriptionTV;
    @NonNull
    public final android.support.v7.widget.RecyclerView viewholderCardItemExpandlistRV;
    @NonNull
    public final android.widget.ImageView viewholderCardItemIV;
    @NonNull
    public final android.widget.LinearLayout viewholderCardItemMainLL;
    @NonNull
    public final android.widget.TextView viewholderCardItemTitleTV;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ViewholderCardItemBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds);
        this.mboundView0 = (android.support.v7.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.viewholderCardItemAlcoholTV = (android.widget.TextView) bindings[4];
        this.viewholderCardItemArrowIV = (android.widget.ImageView) bindings[6];
        this.viewholderCardItemDescriptionTV = (android.widget.TextView) bindings[5];
        this.viewholderCardItemExpandlistRV = (android.support.v7.widget.RecyclerView) bindings[7];
        this.viewholderCardItemIV = (android.widget.ImageView) bindings[2];
        this.viewholderCardItemMainLL = (android.widget.LinearLayout) bindings[1];
        this.viewholderCardItemTitleTV = (android.widget.TextView) bindings[3];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ViewholderCardItemBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ViewholderCardItemBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ViewholderCardItemBinding>inflate(inflater, com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_card_item, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ViewholderCardItemBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ViewholderCardItemBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.viewholder_card_item, null, false), bindingComponent);
    }
    @NonNull
    public static ViewholderCardItemBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ViewholderCardItemBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/viewholder_card_item_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ViewholderCardItemBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}