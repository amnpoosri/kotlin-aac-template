// Generated by Dagger (https://google.github.io/dagger).
package com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment;

import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepository;
import dagger.MembersInjector;
import javax.inject.Provider;

public final class LoginFragmentViewModel_MembersInjector
    implements MembersInjector<LoginFragmentViewModel> {
  private final Provider<LoginRepository> loginRepositoryProvider;

  public LoginFragmentViewModel_MembersInjector(Provider<LoginRepository> loginRepositoryProvider) {
    this.loginRepositoryProvider = loginRepositoryProvider;
  }

  public static MembersInjector<LoginFragmentViewModel> create(
      Provider<LoginRepository> loginRepositoryProvider) {
    return new LoginFragmentViewModel_MembersInjector(loginRepositoryProvider);
  }

  @Override
  public void injectMembers(LoginFragmentViewModel instance) {
    injectLoginRepository(instance, loginRepositoryProvider.get());
  }

  public static void injectLoginRepository(
      LoginFragmentViewModel instance, LoginRepository loginRepository) {
    instance.loginRepository = loginRepository;
  }
}
