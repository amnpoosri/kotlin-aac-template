package com.apdevboost.amnuaychaipoosri.kotlintemplate.local;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class AppDatabase_Impl extends AppDatabase {
  private volatile DrinkDao _drinkDao;

  private volatile DrinkIngredientDao _drinkIngredientDao;

  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `drink` (`drinkId` INTEGER, `drinkImageUrl` TEXT, `drinkAlcoholdContent` TEXT, `drinkTitle` TEXT, `drinkDescription` TEXT, PRIMARY KEY(`drinkId`))");
        _db.execSQL("CREATE  INDEX `index_drink_drinkId` ON `drink` (`drinkId`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `drink_ingredient` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `drink_id` INTEGER, `drinkIngredientId` INTEGER, `drinkIngredientName` TEXT, `drinkIngredientWeight` TEXT, FOREIGN KEY(`drink_id`) REFERENCES `drink`(`drinkId`) ON UPDATE NO ACTION ON DELETE CASCADE )");
        _db.execSQL("CREATE  INDEX `index_drink_ingredient_id` ON `drink_ingredient` (`id`)");
        _db.execSQL("CREATE  INDEX `index_drink_ingredient_drink_id` ON `drink_ingredient` (`drink_id`)");
        _db.execSQL("CREATE  INDEX `index_drink_ingredient_drinkIngredientId` ON `drink_ingredient` (`drinkIngredientId`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"a5256e64ee1ddd3a3a482e38ba91b04c\")");
      }

      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `drink`");
        _db.execSQL("DROP TABLE IF EXISTS `drink_ingredient`");
      }

      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        _db.execSQL("PRAGMA foreign_keys = ON");
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsDrink = new HashMap<String, TableInfo.Column>(5);
        _columnsDrink.put("drinkId", new TableInfo.Column("drinkId", "INTEGER", false, 1));
        _columnsDrink.put("drinkImageUrl", new TableInfo.Column("drinkImageUrl", "TEXT", false, 0));
        _columnsDrink.put("drinkAlcoholdContent", new TableInfo.Column("drinkAlcoholdContent", "TEXT", false, 0));
        _columnsDrink.put("drinkTitle", new TableInfo.Column("drinkTitle", "TEXT", false, 0));
        _columnsDrink.put("drinkDescription", new TableInfo.Column("drinkDescription", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysDrink = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesDrink = new HashSet<TableInfo.Index>(1);
        _indicesDrink.add(new TableInfo.Index("index_drink_drinkId", false, Arrays.asList("drinkId")));
        final TableInfo _infoDrink = new TableInfo("drink", _columnsDrink, _foreignKeysDrink, _indicesDrink);
        final TableInfo _existingDrink = TableInfo.read(_db, "drink");
        if (! _infoDrink.equals(_existingDrink)) {
          throw new IllegalStateException("Migration didn't properly handle drink(com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink).\n"
                  + " Expected:\n" + _infoDrink + "\n"
                  + " Found:\n" + _existingDrink);
        }
        final HashMap<String, TableInfo.Column> _columnsDrinkIngredient = new HashMap<String, TableInfo.Column>(5);
        _columnsDrinkIngredient.put("id", new TableInfo.Column("id", "INTEGER", false, 1));
        _columnsDrinkIngredient.put("drink_id", new TableInfo.Column("drink_id", "INTEGER", false, 0));
        _columnsDrinkIngredient.put("drinkIngredientId", new TableInfo.Column("drinkIngredientId", "INTEGER", false, 0));
        _columnsDrinkIngredient.put("drinkIngredientName", new TableInfo.Column("drinkIngredientName", "TEXT", false, 0));
        _columnsDrinkIngredient.put("drinkIngredientWeight", new TableInfo.Column("drinkIngredientWeight", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysDrinkIngredient = new HashSet<TableInfo.ForeignKey>(1);
        _foreignKeysDrinkIngredient.add(new TableInfo.ForeignKey("drink", "CASCADE", "NO ACTION",Arrays.asList("drink_id"), Arrays.asList("drinkId")));
        final HashSet<TableInfo.Index> _indicesDrinkIngredient = new HashSet<TableInfo.Index>(3);
        _indicesDrinkIngredient.add(new TableInfo.Index("index_drink_ingredient_id", false, Arrays.asList("id")));
        _indicesDrinkIngredient.add(new TableInfo.Index("index_drink_ingredient_drink_id", false, Arrays.asList("drink_id")));
        _indicesDrinkIngredient.add(new TableInfo.Index("index_drink_ingredient_drinkIngredientId", false, Arrays.asList("drinkIngredientId")));
        final TableInfo _infoDrinkIngredient = new TableInfo("drink_ingredient", _columnsDrinkIngredient, _foreignKeysDrinkIngredient, _indicesDrinkIngredient);
        final TableInfo _existingDrinkIngredient = TableInfo.read(_db, "drink_ingredient");
        if (! _infoDrinkIngredient.equals(_existingDrinkIngredient)) {
          throw new IllegalStateException("Migration didn't properly handle drink_ingredient(com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient).\n"
                  + " Expected:\n" + _infoDrinkIngredient + "\n"
                  + " Found:\n" + _existingDrinkIngredient);
        }
      }
    }, "a5256e64ee1ddd3a3a482e38ba91b04c");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "drink","drink_ingredient");
  }

  @Override
  public DrinkDao drinkDao() {
    if (_drinkDao != null) {
      return _drinkDao;
    } else {
      synchronized(this) {
        if(_drinkDao == null) {
          _drinkDao = new DrinkDao_Impl(this);
        }
        return _drinkDao;
      }
    }
  }

  @Override
  public DrinkIngredientDao drinkIngredientDao() {
    if (_drinkIngredientDao != null) {
      return _drinkIngredientDao;
    } else {
      synchronized(this) {
        if(_drinkIngredientDao == null) {
          _drinkIngredientDao = new DrinkIngredientDao_Impl(this);
        }
        return _drinkIngredientDao;
      }
    }
  }
}
