package com.apdevboost.amnuaychaipoosri.kotlintemplate.local;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DrinkIngredientDao_Impl implements DrinkIngredientDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfDrinkIngredient;

  public DrinkIngredientDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfDrinkIngredient = new EntityInsertionAdapter<DrinkIngredient>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `drink_ingredient`(`id`,`drink_id`,`drinkIngredientId`,`drinkIngredientName`,`drinkIngredientWeight`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, DrinkIngredient value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getDrinkId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindLong(2, value.getDrinkId());
        }
        if (value.getDrinkIngredientId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, value.getDrinkIngredientId());
        }
        if (value.getDrinkIngredientName() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDrinkIngredientName());
        }
        if (value.getDrinkIngredientWeight() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getDrinkIngredientWeight());
        }
      }
    };
  }

  @Override
  public void insertOrUpdateDrinkIngredientList(DrinkIngredient... drinkIngredientList) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfDrinkIngredient.insert(drinkIngredientList);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<DrinkIngredient>> getDrinkIngredientList(Long drinkId) {
    final String _sql = "SELECT * FROM drink_ingredient WHERE id = (?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (drinkId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindLong(_argIndex, drinkId);
    }
    return new ComputableLiveData<List<DrinkIngredient>>() {
      private Observer _observer;

      @Override
      protected List<DrinkIngredient> compute() {
        if (_observer == null) {
          _observer = new Observer("drink_ingredient") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfDrinkId = _cursor.getColumnIndexOrThrow("drink_id");
          final int _cursorIndexOfDrinkIngredientId = _cursor.getColumnIndexOrThrow("drinkIngredientId");
          final int _cursorIndexOfDrinkIngredientName = _cursor.getColumnIndexOrThrow("drinkIngredientName");
          final int _cursorIndexOfDrinkIngredientWeight = _cursor.getColumnIndexOrThrow("drinkIngredientWeight");
          final List<DrinkIngredient> _result = new ArrayList<DrinkIngredient>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final DrinkIngredient _item;
            _item = new DrinkIngredient();
            final Long _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getLong(_cursorIndexOfId);
            }
            _item.setId(_tmpId);
            final Long _tmpDrinkId;
            if (_cursor.isNull(_cursorIndexOfDrinkId)) {
              _tmpDrinkId = null;
            } else {
              _tmpDrinkId = _cursor.getLong(_cursorIndexOfDrinkId);
            }
            _item.setDrinkId(_tmpDrinkId);
            final Integer _tmpDrinkIngredientId;
            if (_cursor.isNull(_cursorIndexOfDrinkIngredientId)) {
              _tmpDrinkIngredientId = null;
            } else {
              _tmpDrinkIngredientId = _cursor.getInt(_cursorIndexOfDrinkIngredientId);
            }
            _item.setDrinkIngredientId(_tmpDrinkIngredientId);
            final String _tmpDrinkIngredientName;
            _tmpDrinkIngredientName = _cursor.getString(_cursorIndexOfDrinkIngredientName);
            _item.setDrinkIngredientName(_tmpDrinkIngredientName);
            final String _tmpDrinkIngredientWeight;
            _tmpDrinkIngredientWeight = _cursor.getString(_cursorIndexOfDrinkIngredientWeight);
            _item.setDrinkIngredientWeight(_tmpDrinkIngredientWeight);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }
}
