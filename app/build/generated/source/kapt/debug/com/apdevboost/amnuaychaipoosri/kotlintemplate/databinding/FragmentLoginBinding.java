package com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentLoginBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragmentLoginMainLL, 1);
        sViewsWithIds.put(R.id.fragmentLoginUsernameEdt, 2);
        sViewsWithIds.put(R.id.fragmentLoginPasswordEdt, 3);
        sViewsWithIds.put(R.id.fragmentLoginSubmitBtn, 4);
        sViewsWithIds.put(R.id.fragmentLoginForgotTV, 5);
    }
    // views
    @NonNull
    public final android.widget.TextView fragmentLoginForgotTV;
    @NonNull
    public final android.widget.LinearLayout fragmentLoginMainLL;
    @NonNull
    public final android.widget.EditText fragmentLoginPasswordEdt;
    @NonNull
    public final android.widget.ScrollView fragmentLoginScrollView;
    @NonNull
    public final android.support.v7.widget.AppCompatButton fragmentLoginSubmitBtn;
    @NonNull
    public final android.widget.EditText fragmentLoginUsernameEdt;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentLoginBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds);
        this.fragmentLoginForgotTV = (android.widget.TextView) bindings[5];
        this.fragmentLoginMainLL = (android.widget.LinearLayout) bindings[1];
        this.fragmentLoginPasswordEdt = (android.widget.EditText) bindings[3];
        this.fragmentLoginScrollView = (android.widget.ScrollView) bindings[0];
        this.fragmentLoginScrollView.setTag(null);
        this.fragmentLoginSubmitBtn = (android.support.v7.widget.AppCompatButton) bindings[4];
        this.fragmentLoginUsernameEdt = (android.widget.EditText) bindings[2];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static FragmentLoginBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentLoginBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentLoginBinding>inflate(inflater, com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.fragment_login, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static FragmentLoginBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentLoginBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.apdevboost.amnuaychaipoosri.kotlintemplate.R.layout.fragment_login, null, false), bindingComponent);
    }
    @NonNull
    public static FragmentLoginBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentLoginBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_login_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentLoginBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}