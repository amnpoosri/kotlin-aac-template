package com.apdevboost.amnuaychaipoosri.kotlintemplate.local;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DrinkDao_Impl implements DrinkDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfDrink;

  private final EntityInsertionAdapter __insertionAdapterOfDrinkIngredient;

  public DrinkDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfDrink = new EntityInsertionAdapter<Drink>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `drink`(`drinkId`,`drinkImageUrl`,`drinkAlcoholdContent`,`drinkTitle`,`drinkDescription`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Drink value) {
        if (value.getDrinkId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getDrinkId());
        }
        if (value.getDrinkImageUrl() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDrinkImageUrl());
        }
        if (value.getDrinkAlcoholdContent() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getDrinkAlcoholdContent());
        }
        if (value.getDrinkTitle() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDrinkTitle());
        }
        if (value.getDrinkDescription() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getDrinkDescription());
        }
      }
    };
    this.__insertionAdapterOfDrinkIngredient = new EntityInsertionAdapter<DrinkIngredient>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `drink_ingredient`(`id`,`drink_id`,`drinkIngredientId`,`drinkIngredientName`,`drinkIngredientWeight`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, DrinkIngredient value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getDrinkId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindLong(2, value.getDrinkId());
        }
        if (value.getDrinkIngredientId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, value.getDrinkIngredientId());
        }
        if (value.getDrinkIngredientName() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDrinkIngredientName());
        }
        if (value.getDrinkIngredientWeight() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getDrinkIngredientWeight());
        }
      }
    };
  }

  @Override
  public void insertOrUpdateDrinkList(Drink... drinkList) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfDrink.insert(drinkList);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertOrUpdateDrinkIngredientList(DrinkIngredient... drinkIngredientList) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfDrinkIngredient.insert(drinkIngredientList);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<Drink>> getAlldrinkList() {
    final String _sql = "SELECT * FROM drink";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Drink>>() {
      private Observer _observer;

      @Override
      protected List<Drink> compute() {
        if (_observer == null) {
          _observer = new Observer("drink") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfDrinkId = _cursor.getColumnIndexOrThrow("drinkId");
          final int _cursorIndexOfDrinkImageUrl = _cursor.getColumnIndexOrThrow("drinkImageUrl");
          final int _cursorIndexOfDrinkAlcoholdContent = _cursor.getColumnIndexOrThrow("drinkAlcoholdContent");
          final int _cursorIndexOfDrinkTitle = _cursor.getColumnIndexOrThrow("drinkTitle");
          final int _cursorIndexOfDrinkDescription = _cursor.getColumnIndexOrThrow("drinkDescription");
          final List<Drink> _result = new ArrayList<Drink>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Drink _item;
            _item = new Drink();
            final Long _tmpDrinkId;
            if (_cursor.isNull(_cursorIndexOfDrinkId)) {
              _tmpDrinkId = null;
            } else {
              _tmpDrinkId = _cursor.getLong(_cursorIndexOfDrinkId);
            }
            _item.setDrinkId(_tmpDrinkId);
            final String _tmpDrinkImageUrl;
            _tmpDrinkImageUrl = _cursor.getString(_cursorIndexOfDrinkImageUrl);
            _item.setDrinkImageUrl(_tmpDrinkImageUrl);
            final String _tmpDrinkAlcoholdContent;
            _tmpDrinkAlcoholdContent = _cursor.getString(_cursorIndexOfDrinkAlcoholdContent);
            _item.setDrinkAlcoholdContent(_tmpDrinkAlcoholdContent);
            final String _tmpDrinkTitle;
            _tmpDrinkTitle = _cursor.getString(_cursorIndexOfDrinkTitle);
            _item.setDrinkTitle(_tmpDrinkTitle);
            final String _tmpDrinkDescription;
            _tmpDrinkDescription = _cursor.getString(_cursorIndexOfDrinkDescription);
            _item.setDrinkDescription(_tmpDrinkDescription);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<Drink> loadProduct(int drinkId) {
    final String _sql = "SELECT * FROM drink WHERE drinkId = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, drinkId);
    return new ComputableLiveData<Drink>() {
      private Observer _observer;

      @Override
      protected Drink compute() {
        if (_observer == null) {
          _observer = new Observer("drink") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfDrinkId = _cursor.getColumnIndexOrThrow("drinkId");
          final int _cursorIndexOfDrinkImageUrl = _cursor.getColumnIndexOrThrow("drinkImageUrl");
          final int _cursorIndexOfDrinkAlcoholdContent = _cursor.getColumnIndexOrThrow("drinkAlcoholdContent");
          final int _cursorIndexOfDrinkTitle = _cursor.getColumnIndexOrThrow("drinkTitle");
          final int _cursorIndexOfDrinkDescription = _cursor.getColumnIndexOrThrow("drinkDescription");
          final Drink _result;
          if(_cursor.moveToFirst()) {
            _result = new Drink();
            final Long _tmpDrinkId;
            if (_cursor.isNull(_cursorIndexOfDrinkId)) {
              _tmpDrinkId = null;
            } else {
              _tmpDrinkId = _cursor.getLong(_cursorIndexOfDrinkId);
            }
            _result.setDrinkId(_tmpDrinkId);
            final String _tmpDrinkImageUrl;
            _tmpDrinkImageUrl = _cursor.getString(_cursorIndexOfDrinkImageUrl);
            _result.setDrinkImageUrl(_tmpDrinkImageUrl);
            final String _tmpDrinkAlcoholdContent;
            _tmpDrinkAlcoholdContent = _cursor.getString(_cursorIndexOfDrinkAlcoholdContent);
            _result.setDrinkAlcoholdContent(_tmpDrinkAlcoholdContent);
            final String _tmpDrinkTitle;
            _tmpDrinkTitle = _cursor.getString(_cursorIndexOfDrinkTitle);
            _result.setDrinkTitle(_tmpDrinkTitle);
            final String _tmpDrinkDescription;
            _tmpDrinkDescription = _cursor.getString(_cursorIndexOfDrinkDescription);
            _result.setDrinkDescription(_tmpDrinkDescription);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<Drink>> getAllDrinkWithDrinkIngredient() {
    final String _sql = "SELECT * FROM drink INNER JOIN drink_ingredient ON drinkId";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Drink>>() {
      private Observer _observer;

      @Override
      protected List<Drink> compute() {
        if (_observer == null) {
          _observer = new Observer("drink","drink_ingredient") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfDrinkId = _cursor.getColumnIndexOrThrow("drinkId");
          final int _cursorIndexOfDrinkImageUrl = _cursor.getColumnIndexOrThrow("drinkImageUrl");
          final int _cursorIndexOfDrinkAlcoholdContent = _cursor.getColumnIndexOrThrow("drinkAlcoholdContent");
          final int _cursorIndexOfDrinkTitle = _cursor.getColumnIndexOrThrow("drinkTitle");
          final int _cursorIndexOfDrinkDescription = _cursor.getColumnIndexOrThrow("drinkDescription");
          final List<Drink> _result = new ArrayList<Drink>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Drink _item;
            _item = new Drink();
            final Long _tmpDrinkId;
            if (_cursor.isNull(_cursorIndexOfDrinkId)) {
              _tmpDrinkId = null;
            } else {
              _tmpDrinkId = _cursor.getLong(_cursorIndexOfDrinkId);
            }
            _item.setDrinkId(_tmpDrinkId);
            final String _tmpDrinkImageUrl;
            _tmpDrinkImageUrl = _cursor.getString(_cursorIndexOfDrinkImageUrl);
            _item.setDrinkImageUrl(_tmpDrinkImageUrl);
            final String _tmpDrinkAlcoholdContent;
            _tmpDrinkAlcoholdContent = _cursor.getString(_cursorIndexOfDrinkAlcoholdContent);
            _item.setDrinkAlcoholdContent(_tmpDrinkAlcoholdContent);
            final String _tmpDrinkTitle;
            _tmpDrinkTitle = _cursor.getString(_cursorIndexOfDrinkTitle);
            _item.setDrinkTitle(_tmpDrinkTitle);
            final String _tmpDrinkDescription;
            _tmpDrinkDescription = _cursor.getString(_cursorIndexOfDrinkDescription);
            _item.setDrinkDescription(_tmpDrinkDescription);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }
}
