package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom

import android.support.v7.widget.RecyclerView
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderEmptyBinding

class EmptyViewHolder(val binding: ViewholderEmptyBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            binding.executePendingBindings()
        }
    }