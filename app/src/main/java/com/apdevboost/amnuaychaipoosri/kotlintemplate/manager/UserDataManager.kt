package com.apdevboost.amnuaychaipoosri.kotlintemplate.manager

import android.content.SharedPreferences
import javax.inject.Singleton

@Singleton
class UserDataManager(private val pref: SharedPreferences, private val editor: SharedPreferences.Editor) {

    private val KEY_USER_TOKEN = "user_token"

    fun setUserToken(token: String?) {
        editor.putString(KEY_USER_TOKEN, token)
        editor.apply()
    }

    fun getUserToken(): String = pref.getString(KEY_USER_TOKEN, "")

    fun isUserLogin(): Boolean {
        return getUserToken().isNotEmpty()
    }


}