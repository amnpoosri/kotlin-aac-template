package com.apdevboost.amnuaychaipoosri.kotlintemplate.constant

class Constants {
    companion object {

        const val APP = "KotlinACCTemplate"
        const val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"

        const val DEVELOPMENT_BASE_URL = "http://www.mocky.io/"
        const val TESTING_BASE_URL = "http://www.mocky.io/"
        const val PRODUCTION_BASE_URL = "http://www.mocky.io/"

        const val DB_FILE_NAME = "common_db"
        const val SHARE_PREF_NAME = "local"
    }
}