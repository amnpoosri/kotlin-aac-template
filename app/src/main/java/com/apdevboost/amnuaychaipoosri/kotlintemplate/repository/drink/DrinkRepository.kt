package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink

import android.arch.lifecycle.LiveData
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink

interface DrinkRepository {

    fun getDrinkList(isForceFetch:Boolean): LiveData<Resource<List<Drink>>>

    fun getDrinkListNoRoom(isForceFetch:Boolean): LiveData<Resource<List<Drink>>>

    fun deleteDrinkDatabase()

}