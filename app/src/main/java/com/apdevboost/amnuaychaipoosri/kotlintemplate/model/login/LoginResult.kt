package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResult {

    @SerializedName("token")
    @Expose
    var token: String? = null


    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

}