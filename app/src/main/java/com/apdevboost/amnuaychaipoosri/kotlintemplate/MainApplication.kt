package com.apdevboost.amnuaychaipoosri.kotlintemplate

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.apdevboost.amnuaychaipoosri.kotlintemplate.component.AppComponent
import com.apdevboost.amnuaychaipoosri.kotlintemplate.component.DaggerAppComponent
import com.apdevboost.amnuaychaipoosri.kotlintemplate.constant.Constants
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.*

class MainApplication: MultiDexApplication() {

    companion object {
        //platformStatic allow access it from java code
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .localDataModule(LocalDataModule(this))
                .remoteDataModule(RemoteDataModule(Constants.DEVELOPMENT_BASE_URL))
                .appExecutorsModule(AppExecutorsModule())
                .repositoryModule(RepositoryModule())
                .configurationModule(ConfigurationModule())
                .build()
        appComponent.inject(this)
    }
}