package com.apdevboost.amnuaychaipoosri.kotlintemplate.remote

import android.arch.lifecycle.LiveData
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.ApiResponse
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.BaseResponse
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult
import retrofit2.http.GET
import retrofit2.http.Query


interface LoginApi {

    @GET("v2/5ae41f342f00004b0028e771")
    fun login(@Query("username") username: String, @Query("password") password: String): LiveData<ApiResponse<BaseResponse<LoginResult>>>
}