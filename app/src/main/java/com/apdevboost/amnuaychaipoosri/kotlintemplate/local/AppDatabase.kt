package com.apdevboost.amnuaychaipoosri.kotlintemplate.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient
import com.apdevboost.amnuaychaipoosri.kotlintemplate.utils.Converters

@Database(entities = [
    (Drink::class),
    (DrinkIngredient::class)
    ], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun drinkDao(): DrinkDao
    abstract fun drinkIngredientDao(): DrinkIngredientDao

}

