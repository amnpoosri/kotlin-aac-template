package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
