package com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepository
import javax.inject.Inject

class LoginFragmentViewModel: ViewModel() {

    @Inject
    lateinit var loginRepository: LoginRepository

    fun validateData(username: String, password: String): Boolean {
        if (username == null || password == null) {
            return false
        }

        if (username.isEmpty() || password.isEmpty()) {
            return false
        }

        if (!emailValidator(username)) {
            return false
        }

        return true
    }

    fun login(username: String, password: String): LiveData<Resource<LoginResult>> {
        if (validateData(username, password))
            {

            }
        return loginRepository.login(false, username, password)
    }

    private fun emailValidator(email: String) : Boolean {
        return if (email.isNotEmpty()) {
            android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        } else{
            false
        }
    }

}