package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.apdevboost.amnuaychaipoosri.kotlintemplate.MainApplication
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivityMainBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Status
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.base.BaseActivity
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.adapter.DrinkRecyclerViewAdapter
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.MainViewModel
import java.util.Collections.shuffle

class MainActivity : BaseActivity() {


    private val TAG = "MainActivity"
    private val adapter = DrinkRecyclerViewAdapter()

    private val mainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java).also {
            MainApplication.appComponent.inject(it)
        }
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            onRestoreSavedInstanceState(savedInstanceState)
        } else {
            onRetrievedDataFromBundle(intent)
        }
        super.onCreate(savedInstanceState)
        initial()
    }

    private fun onRestoreSavedInstanceState(savedInstanceState: Bundle) {
//        id = savedInstanceState.getInt("id")
    }

    private fun onRetrievedDataFromBundle(intent: Intent?) {
//        id = intent?.getIntExtra("id", 0) ?: 0
    }

    override fun onSaveInstanceState(outState: Bundle?) {
//        outState?.putInt("id", id)
        super.onSaveInstanceState(outState)
    }

    private fun initial() {

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setSupportActionBar(binding.activityMainToolbar)

        binding.activityMainRV.layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        binding.activityMainRV.adapter = adapter

        binding.activityMainSwipeLayout.setOnRefreshListener {
            if (adapter.getAdapterItems() != null)
            {
                var shuffleDrinkList =  adapter.getAdapterItems()!!.toMutableList()
                shuffle(shuffleDrinkList)
                updateDrinkAdapter(ArrayList(shuffleDrinkList))
            }
        binding.activityMainSwipeLayout?.isRefreshing = false

        }

        getDrinkListData()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.toolbar_menu_logout -> {
                mainViewModel.logout()
                attemptToGoLoginPage()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun attemptToGoLoginPage() {
        if(!mainViewModel.isUserLogin()) {
            Toast.makeText(this, "Successfully Logout", Toast.LENGTH_SHORT).show()
            goToLoginPage()
        }
        else
            Toast.makeText(this, "Logout Unsuccessful", Toast.LENGTH_SHORT).show()
    }

    private fun goToLoginPage()
    {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun getDrinkListData() {

        mainViewModel.getDrinkListData().observe(this, Observer {
            if (it != null) {
                onGetDrinkListDataResult(it)
            }
        })
    }

    private fun updateDrinkAdapter(data: ArrayList<Drink>) {
        adapter.submitList(data)
        }

    private fun onGetDrinkListDataResult(data: Resource<List<Drink>>?) {
        if (data != null) {
            when {
                data.status == Status.SUCCESS -> {
                    binding.loading?.loadingWidget?.visibility = View.GONE
                    if (data.data!=null) {
                        adapter.setAdapterItems(ArrayList(data.data))
                        updateDrinkAdapter(ArrayList(data.data))
                    }
                }
                data.status == Status.ERROR -> {
                    binding.loading?.loadingWidget?.visibility = View.GONE
                    Toast.makeText(this, "Something went wrong.", Toast.LENGTH_SHORT).show()
                    // TODO: Handle Error
                }
                data.status == Status.LOADING -> {
                    binding.loading?.loadingWidget?.visibility = View.VISIBLE
                }
            }
        }
    }


}