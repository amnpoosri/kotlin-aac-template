package com.apdevboost.amnuaychaipoosri.kotlintemplate.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient

@Dao
interface DrinkDao {

    //Drink list table
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateDrinkList(vararg drinkList: Drink)

    @Query("SELECT * FROM drink")
    fun getAlldrinkList(): LiveData<List<Drink>>

    @Query("SELECT * FROM drink WHERE drinkId = :drinkId")
    fun loadProduct(drinkId: Int): LiveData<Drink>

    @Query("SELECT * FROM drink INNER JOIN drink_ingredient ON drinkId")
    fun getAllDrinkWithDrinkIngredient(): LiveData<List<Drink>>

    //Drink Ingredient list table
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateDrinkIngredientList(vararg drinkIngredientList: DrinkIngredient)

}