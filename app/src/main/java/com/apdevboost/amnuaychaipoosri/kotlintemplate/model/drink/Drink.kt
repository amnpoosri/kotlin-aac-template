package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink

import android.arch.persistence.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



@Entity(tableName = "drink", indices = [(Index(value = "drinkId"))])
class Drink {

    @PrimaryKey
    @ColumnInfo(name = "drinkId")
    @SerializedName("drinkId")
    @Expose
    var drinkId: Long? = null

    @SerializedName("drinkImageUrl")
    @Expose
    var drinkImageUrl: String? = null

    @SerializedName("drinkAlcoholContent")
    @Expose
    var drinkAlcoholdContent: String? = null

    @SerializedName("drinkTitle")
    @Expose
    var drinkTitle: String? = null

    @SerializedName("drinkDescription")
    @Expose
    var drinkDescription: String? =null

    @Ignore
    @SerializedName("drinkIngredientList")
    @Expose
    var drinkIngredientList: List<DrinkIngredient>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param drinkId
     * @param drinkImageUrl
     * @param drinkTitle
     * @param drinkDescription
     * @param drinkIngredientList
     */
    constructor(drinkId: Long?, drinkImageUrl: String, drinkTitle: String?, drinkDescription: String?, drinkIngredientList: List<DrinkIngredient>?) : super() {
        this.drinkId = drinkId
        this.drinkImageUrl = drinkImageUrl
        this.drinkTitle = drinkTitle
        this.drinkDescription = drinkDescription
        this.drinkIngredientList = drinkIngredientList
    }

}