package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink

import android.arch.persistence.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "drink_ingredient",
        foreignKeys = [(ForeignKey(entity = Drink::class,
                parentColumns = arrayOf("drinkId"),
                childColumns = arrayOf("drink_id"),
                onDelete = ForeignKey.CASCADE))],
        indices = [(Index(value = "id")), (Index(value = "drink_id")), (Index(value = "drinkIngredientId"))])

class DrinkIngredient {

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null

    @ColumnInfo(name = "drink_id")
    var drinkId: Long? = null

    @ColumnInfo(name = "drinkIngredientId")
    @SerializedName("drinkIngredientId")
    @Expose
    var drinkIngredientId:Int? = null

    @SerializedName("drinkIngredientName")
    @Expose
    var drinkIngredientName:String? = null

    @SerializedName("drinkIngredientWeight")
    @Expose
    var drinkIngredientWeight: String? = null



}