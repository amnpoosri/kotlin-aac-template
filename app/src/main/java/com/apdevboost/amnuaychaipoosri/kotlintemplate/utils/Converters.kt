package com.apdevboost.amnuaychaipoosri.kotlintemplate.utils

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*


public class Converters {
    @TypeConverter
    fun fromString(value: String): ArrayList<String>? {
        val listType = object : TypeToken<ArrayList<String>>() {

        }.type
        return Gson().fromJson<ArrayList<String>>(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }


    @TypeConverter
    fun fromHashMap(hashMap:HashMap<String, String>): String {
        val gson = Gson()
        return gson.toJson(hashMap)
    }

    @TypeConverter
    fun toHashMap(value: String): HashMap<String, String>? {
        val hashMapType = object : TypeToken<HashMap<String, String>>() {

        }.type
        return Gson().fromJson<HashMap<String, String>>(value, hashMapType)
    }

    @TypeConverter
    fun toDate(timestamp: Long?): Date? {
        return if (timestamp == null) null else Date(timestamp)
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return (date?.time)?.toLong()
    }

//    @TypeConverter
//    fun fromGeoPoint(point: GeoPoint?): String? {
//        return "" + point?.latitude + "," + point?.longitude
//    }
//
//    @TypeConverter
//    fun toGeoPoint(latlng: String?): GeoPoint? {
//        if (latlng == null || latlng.contains("null")) return null
//        val splited = latlng.split(",")
//        return GeoPoint(splited[0].toDouble(), splited[1].toDouble())
//    }

}