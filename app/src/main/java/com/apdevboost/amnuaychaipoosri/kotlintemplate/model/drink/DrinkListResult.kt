package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DrinkListResult {

    @SerializedName("drinkList")
    @Expose
    var drinkList: List<Drink>? = null


    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

}