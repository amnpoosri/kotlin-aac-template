package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.fragment

import android.app.Activity
import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.apdevboost.amnuaychaipoosri.kotlintemplate.MainApplication
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.FragmentLoginBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.fragment.base.BaseFragment
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Status
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.MainActivity
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog.SingleButtonDialog
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment.LoginFragmentViewModel


class LoginFragment : BaseFragment(), SingleButtonDialog.DialogSingleButtonClickListener {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var loginfragmentViewModel : LoginFragmentViewModel
    private lateinit var progressDialog : ProgressDialog
    fun newInstance(): LoginFragment {
        val fragment = LoginFragment()
        val bundle = Bundle()
        fragment.arguments = bundle
        return fragment
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            DataBindingUtil.inflate<FragmentLoginBinding>(inflater, R.layout.fragment_login, container, false).also {
                binding = it
            }.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initial()
    }

    private fun initial() {
        progressDialog = ProgressDialog(context)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Authenticating...")

        loginfragmentViewModel = ViewModelProviders.of(this).get(LoginFragmentViewModel::class.java).also {
            MainApplication.appComponent.inject(it)
        }

        binding.fragmentLoginMainLL.setOnClickListener {
            hideKeyboard(it)
        }
        binding.fragmentLoginUsernameEdt.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        binding.fragmentLoginPasswordEdt.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        binding.fragmentLoginSubmitBtn?.setOnClickListener {
            loginfragmentViewModel.login(binding.fragmentLoginUsernameEdt.text.toString(), binding.fragmentLoginPasswordEdt.text.toString())
                    .observe(this, Observer<Resource<LoginResult>> { if (it!=null) onLoginResult(it) })
        }

        binding.fragmentLoginForgotTV?.setOnClickListener {
            val dialog = SingleButtonDialog.Builder()
                    .request(0)
                    .setTitle(getString(R.string.forgot_your_password))
                    .setSubTitle(getString(R.string.you_dont_need_a_password))
                    .setbuttonName(getString(R.string.ok))
                    .setOnButtonClickListener(this)
                    .build()
            dialog.show(childFragmentManager, "Forgot Dialog")
        }
    }


    private fun setUsernameError(errorText: String) {
        binding.fragmentLoginUsernameEdt.error = errorText
    }

    private fun setPasswordError(errorText: String) {
        binding.fragmentLoginPasswordEdt.error = errorText
    }
    private fun onLoginResult(result: Resource<LoginResult>) {
        when {
            result.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (result.data?.token!=null) {
                    Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show()
                    goToMainPage()
                }
            }
            result.status == Status.ERROR -> {
                progressDialog.dismiss()
                Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
            }
            result.status == Status.LOADING -> {
                progressDialog.show()
                Log.d("debug", "loading...")
            }
        }
    }

    private fun goToMainPage() {
        context?.startActivity(Intent(context, MainActivity::class.java))
        activity?.finish()
    }

    /**
     * Hides the soft keyboard
     */

    private fun hideKeyboard(view: View) {
        val inputMethodManager = context!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onDialogSingleButtonClickListener(dialog: SingleButtonDialog, v: View?, request: Int) {
        dialog.dismiss()
    }
}