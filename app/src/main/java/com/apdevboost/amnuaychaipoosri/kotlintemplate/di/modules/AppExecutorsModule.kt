package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules

import com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors
import dagger.Module
import dagger.Provides

@Module
class AppExecutorsModule {

    @Provides
    @ApplicationScope
    fun provideAppExecutors(): AppExecutors {
        return AppExecutors.instance
    }

}