package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login

import android.arch.lifecycle.LiveData
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult

interface LoginRepository {

    fun login(isForceFetch: Boolean, username: String, password: String): LiveData<Resource<LoginResult>>

}