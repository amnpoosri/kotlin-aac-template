package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules

import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao
import com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepositoryImpl
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepository
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepositoryImpl
import com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun provideLoginRepository(remoteSource: LoginApi, localSource: UserDataManager): LoginRepository
            = LoginRepositoryImpl(remoteSource, localSource)

    @Provides
    @ApplicationScope
    fun drinkRepositoryImpl(localSource: DrinkDao, localDrinkIngredientSource: DrinkIngredientDao, remoteSource: Api, appExecutors: AppExecutors) : DrinkRepository
            = DrinkRepositoryImpl(localSource, localDrinkIngredientSource, remoteSource, appExecutors)

}

