package com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository
import javax.inject.Inject

class MainViewModel: ViewModel() {

    @Inject
    lateinit var drinkRepo: DrinkRepository

    @Inject
    lateinit var userDataManager: UserDataManager

    fun getDrinkListData(): LiveData<Resource<List<Drink>>> {

//        Get from remote only
//        return drinkRepo.getDrinkListNoRoom(true)
//        Get from remote and store in room
        return drinkRepo.getDrinkList(true)
    }

    fun isUserLogin(): Boolean {
        return userDataManager.isUserLogin()
    }

    fun logout() {
        userDataManager.setUserToken("")
        drinkRepo.deleteDrinkDatabase()
    }


}