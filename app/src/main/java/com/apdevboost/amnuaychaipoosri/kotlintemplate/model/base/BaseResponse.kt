package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base

import com.google.gson.annotations.SerializedName

/**
 * Created by darkkillen on 12/20/2017 AD.
 */
class BaseResponse<T> {

    lateinit var status: String
    lateinit var message: String

    @SerializedName("responseObject")
    val data: T? = null

}