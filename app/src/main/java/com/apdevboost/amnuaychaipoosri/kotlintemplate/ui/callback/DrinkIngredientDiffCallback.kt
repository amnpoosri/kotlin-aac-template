package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.callback

import android.support.v7.util.DiffUtil
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient

class DrinkIngredientDiffCallback : DiffUtil.ItemCallback<DrinkIngredient>() {
    override fun areItemsTheSame(oldItem: DrinkIngredient, newItem: DrinkIngredient): Boolean {
        return oldItem.drinkIngredientId == newItem.drinkIngredientId
    }

    override fun areContentsTheSame(oldItem: DrinkIngredient, newItem: DrinkIngredient): Boolean {
        return oldItem == newItem
    }
}