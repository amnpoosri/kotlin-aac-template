package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.support.annotation.NonNull
import android.util.Log
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.ApiResponse
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.BaseResponse
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkListResult
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.NetworkBoundResource

class DrinkRepositoryImpl(val localSource: DrinkDao, val localDrinkIngredientSource: DrinkIngredientDao, val remoteSource: Api, val appExecutors: AppExecutors) : DrinkRepository {
    override fun getDrinkListNoRoom(isForceFetch: Boolean): LiveData<Resource<List<Drink>>> {
        val result = MediatorLiveData<Resource<List<Drink>>>()
        result.setValue(Resource.loading(null))
        val apiResponse = remoteSource.getDrinkList()
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            if (response!!.isSuccessful) {
                result.addSource(apiResponse) {
                    result.value = Resource.success(response.body?.data!!.drinkList)
                }
            } else {
                result.addSource(apiResponse
                ) { newData -> result.setValue(Resource.error(response.errorMessage!!, newData?.body?.data!!.drinkList)) }
            }
        }
        return result
    }

    override fun getDrinkList(isForceFetch:Boolean): LiveData<Resource<List<Drink>>> {
        return object: NetworkBoundResource<List<Drink>, BaseResponse<DrinkListResult>>(appExecutors) {
            override fun saveCallResult(items: BaseResponse<DrinkListResult>) {
                if (items.data?.drinkList != null && items.data.drinkList!!.isNotEmpty()) {
                    for (i in 0 until items.data.drinkList!!.size) {
                        localSource.insertOrUpdateDrinkList(items.data.drinkList!![i])
                        if (items.data?.drinkList!![i].drinkIngredientList != null && items.data.drinkList!![i].drinkIngredientList!!.isNotEmpty()) {
                            for (j in 0 until items.data.drinkList!![i].drinkIngredientList!!.size) {
                                var drinkIngredient: DrinkIngredient = items.data.drinkList!![i].drinkIngredientList!![j]
                                //Insert null for primary key auto-generate
                                drinkIngredient.id = null
                                drinkIngredient.drinkId = items.data.drinkList!![i].drinkId
                                try {
                                    localSource.insertOrUpdateDrinkIngredientList(drinkIngredient)
                                }
                                catch (e: Exception)
                                {
                                Log.e("Insert Error",""+e)
                                }
                            }

                        }
                    }
                }


            }

            override fun shouldFetch(data:List<Drink>?):Boolean {
                return isForceFetch || data == null
//                return isForceFetch || data == null || userRateLimit.shouldFetch(Constants.FETCH_KEY_USER)
            }
            @NonNull
            override fun loadFromDb():LiveData<List<Drink>> {
                Log.d("Drink","HHHHHH"+ localDrinkIngredientSource.getDrinkIngredientList(1))
                Log.d("Drink","HHHHHH1"+ localSource.getAlldrinkList().value)
                return localSource.getAlldrinkList()
            }
            @NonNull
            override fun createCall(): LiveData<ApiResponse<BaseResponse<DrinkListResult>>>{
                return remoteSource.getDrinkList()
            }

            override fun onFetchFailed() {
//                userRateLimit.reset(Constants.FETCH_KEY_USER)
            }

        }.asLiveData()
    }

    override fun deleteDrinkDatabase() {

    }
    
    fun getDrinksWithIngredients(): LiveData<List<Drink>> {
        var localDrinks = localSource.getAlldrinkList()
//        for (i in 0 until localDrinks.value!!.size) {
//            localDrinks.value!![i].drinkIngredientList = localDrinkIngredientSource.getDrinkIngredientList(localDrinks.value!![i].drinkId)
//        }
        return localDrinks
    }


}
