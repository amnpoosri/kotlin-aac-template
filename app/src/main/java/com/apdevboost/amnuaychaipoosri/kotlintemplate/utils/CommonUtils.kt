package com.apdevboost.amnuaychaipoosri.kotlintemplate.utils

import android.content.Context
import android.provider.Settings
import com.apdevboost.amnuaychaipoosri.kotlintemplate.constant.Constants
import java.text.SimpleDateFormat
import java.util.*


object CommonUtils {

    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getTimestamp(): String {
        return SimpleDateFormat(Constants.TIMESTAMP_FORMAT, Locale.US).format(Date())
    }
    fun parseStringArray(context: Context?, stringArrayResourceId: Int): HashMap<String, String>? {
        if (context == null) return null
        val stringArray = context.resources.getStringArray(stringArrayResourceId)
        val outputArray = HashMap<String, String>()
        stringArray
                .map { it.split("\\|".toRegex(), 2).toTypedArray() }
                .forEach { outputArray.put(it[0], it[1]) }
        return outputArray
    }

}