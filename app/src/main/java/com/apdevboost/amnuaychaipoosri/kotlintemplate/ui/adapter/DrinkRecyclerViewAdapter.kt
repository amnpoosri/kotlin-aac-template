package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.adapter

import android.content.Context
import android.support.v7.recyclerview.extensions.AsyncDifferConfig
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderCardItemBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderEmptyBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.callback.DrinkDiffCallback
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.EmptyViewHolder
import com.bumptech.glide.Glide



open class DrinkRecyclerViewAdapter : ListAdapter<Drink, RecyclerView.ViewHolder>(AsyncDifferConfig.Builder(DrinkDiffCallback()).build()) {
    private val VIEWTYPE_NORMAL: Int = 1
    private var context: Context? = null
    private var drinkList: ArrayList<Drink>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType) {
            VIEWTYPE_NORMAL -> {
                val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
                context = parent.context
                val viewCardItemBinding: ViewholderCardItemBinding = ViewholderCardItemBinding.inflate(layoutInflater, parent, false)
                return CardViewItemHolder(viewCardItemBinding)

            }
            else -> {
                val layoutInflater:LayoutInflater = LayoutInflater.from(parent.context)
                val viewholderEmptyBinding: ViewholderEmptyBinding = ViewholderEmptyBinding.inflate(layoutInflater, parent, false)
                return EmptyViewHolder(viewholderEmptyBinding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return VIEWTYPE_NORMAL
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CardViewItemHolder) {
            holder.bind()

            val drink:Drink = getItem(position)

            val adapter = ExpandlistRecyclerViewAdapter()

            holder.binding.viewholderCardItemExpandlistRV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            holder.binding.viewholderCardItemExpandlistRV.adapter = adapter
            adapter.submitList(drink.drinkIngredientList)

            if (context != null) {
                Glide.with(context!!)
                        .load(drink.drinkImageUrl)
                        .into(holder.binding.viewholderCardItemIV)
            }
            holder.binding.viewholderCardItemTitleTV.text = drink.drinkTitle
            holder.binding.viewholderCardItemAlcoholTV.text = "Alcohol : "+ drink.drinkAlcoholdContent
            holder.binding.viewholderCardItemDescriptionTV.text = drink.drinkDescription

            holder.binding.viewholderCardItemMainLL?.setOnClickListener {
                toggleExpandlist(holder)
            }

        }
    }

    fun toggleExpandlist(holder: CardViewItemHolder)  {
        if (holder is CardViewItemHolder) {
            if (holder.binding.viewholderCardItemExpandlistRV.visibility == View.VISIBLE) {
                holder.binding.viewholderCardItemArrowIV?.setImageResource(R.drawable.ic_arrow_down)
                holder.binding.viewholderCardItemExpandlistRV.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_out))
                holder.binding.viewholderCardItemExpandlistRV.visibility = View.GONE
            }
            else {
                holder.binding.viewholderCardItemArrowIV?.setImageResource(R.drawable.ic_arrow_up)
                holder.binding.viewholderCardItemExpandlistRV.visibility = View.VISIBLE
                holder.binding.viewholderCardItemExpandlistRV.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in))
            }
        }
    }

    fun getAdapterItems(): ArrayList<Drink>?
    {
        return drinkList
    }

    fun setAdapterItems(drinkList: ArrayList<Drink>)
    {
       this.drinkList = drinkList
    }

    inner class CardViewItemHolder(val binding: ViewholderCardItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            binding.executePendingBindings()
        }
    }

}