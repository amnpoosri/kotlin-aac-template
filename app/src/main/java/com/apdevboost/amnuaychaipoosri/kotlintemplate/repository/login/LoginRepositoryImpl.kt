package com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.login.LoginResult
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi

class LoginRepositoryImpl(val remoteSource: LoginApi, val localSource: UserDataManager) : LoginRepository {

    override fun login(isForceFetch: Boolean, username: String, password: String): LiveData<Resource<LoginResult>> {
        val result = MediatorLiveData<Resource<LoginResult>>()
        result.setValue(Resource.loading(null))
        val apiResponse = remoteSource.login(username, password)
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            if (response!!.isSuccessful) {
                result.addSource(apiResponse) {
                    result.value = Resource.success(response.body?.data)
                    localSource.setUserToken(response.body?.data?.token)
                }
            } else {
                result.addSource(apiResponse
                ) { newData -> result.setValue(Resource.error(response.errorMessage!!, newData?.body?.data)) }
            }
        }
        return result
    }

}
