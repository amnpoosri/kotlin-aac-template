package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules


import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi
import com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope
import com.apdevboost.amnuaychaipoosri.kotlintemplate.utils.LiveDataCallAdapterFactory
import com.apdevboost.amnuaychaipoosri.kotlintemplate.utils.PrettyHttpLogger
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class RemoteDataModule(val baseUrl: String) {


    @Provides @ApplicationScope
    fun provideLoginApi(retrofit: Retrofit): LoginApi
            = retrofit.create(LoginApi::class.java)

    @Provides @ApplicationScope
    fun provideApi(retrofit: Retrofit): Api
            = retrofit.create(Api::class.java)

    @Provides @ApplicationScope
    fun provideRetrofit(): Retrofit
            = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().addNetworkInterceptor(getDefaultHttpLoggingInterceptor()).build())
            .build()

    private fun getDefaultHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor(PrettyHttpLogger()).setLevel(HttpLoggingInterceptor.Level.BODY)
    }
}

