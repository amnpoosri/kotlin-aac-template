package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.callback

import android.support.v7.util.DiffUtil
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink

class DrinkDiffCallback : DiffUtil.ItemCallback<Drink>() {
    override fun areItemsTheSame(oldItem: Drink, newItem: Drink): Boolean {
        return oldItem.drinkId == newItem.drinkId
    }

    override fun areContentsTheSame(oldItem: Drink, newItem: Drink): Boolean {
        return oldItem.drinkTitle == newItem.drinkTitle && oldItem.drinkImageUrl == newItem.drinkImageUrl
    }
}