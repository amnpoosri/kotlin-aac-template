package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivityLoginBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.fragment.LoginFragment

class LoginActivity : AppCompatActivity() {

    private val TAG = "LoginActivity"

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initial()
    }

    private fun initial() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        val loginFragment = LoginFragment()
        loginFragment.arguments = intent.extras
        supportFragmentManager.beginTransaction().add(
                R.id.activityLoginContainer, loginFragment).commit()
    }
}