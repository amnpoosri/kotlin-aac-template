package com.apdevboost.amnuaychaipoosri.kotlintemplate.component

import android.app.Application
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.*
import com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope
import com.apdevboost.amnuaychaipoosri.kotlintemplate.template.viewmodel.TemplateViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.MainViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.SplashViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment.LoginFragmentViewModel
import dagger.Component

@ApplicationScope
@Component(
        modules = arrayOf(AppModule::class,
                LocalDataModule::class,
                RemoteDataModule::class,
                AppExecutorsModule::class,
                RepositoryModule::class,
                ConfigurationModule::class)
)
interface AppComponent {
    fun inject(application: Application)
    fun inject(splashViewModel: SplashViewModel)
    fun inject(mainViewModel: MainViewModel)
    fun inject(templateViewModel: TemplateViewModel)
    fun inject(loginFragmentViewModel: LoginFragmentViewModel)
}