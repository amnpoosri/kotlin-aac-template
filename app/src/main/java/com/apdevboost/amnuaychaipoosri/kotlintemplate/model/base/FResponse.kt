package com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base

class FResponse<T>(val isSuccessful:Boolean, val data: T?, val message: String?) {

}