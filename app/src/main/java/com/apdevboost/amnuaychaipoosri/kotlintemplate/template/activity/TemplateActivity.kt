package com.apdevboost.amnuaychaipoosri.kotlintemplate.template.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import com.apdevboost.amnuaychaipoosri.kotlintemplate.MainApplication
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivityTemplateBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.template.viewmodel.TemplateViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.base.BaseActivity

class TemplateActivity : BaseActivity() {

    private val viewModel: TemplateViewModel by lazy {
        ViewModelProviders.of(this).get(TemplateViewModel::class.java).also {
            MainApplication.appComponent.inject(it)
        }
    }

    private lateinit var binding: ActivityTemplateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initial()
    }

    private fun initial() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_template)

    }

}