package com.apdevboost.amnuaychaipoosri.kotlintemplate.utils

import android.arch.lifecycle.LiveData

class AbsentLiveData<T>: LiveData<T>() {
    init {
        postValue(null)
    }

    companion object {
        fun <T> create(): LiveData<T> {

            return AbsentLiveData()
        }
    }
}